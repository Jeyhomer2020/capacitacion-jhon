package aplicacion.estructuraMapeo;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BaseMapeoDom {

	private final WebDriver driver;
	private final WebDriverWait wait;
	private final String pagina;

	public BaseMapeoDom(WebDriver driver, String pagina) {
		this.pagina = pagina;
		this.driver = driver;
		this.driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		this.wait = new WebDriverWait(driver, 60);

	}

	public WebElement getWebElement(String element) {
		try {

			MapeoWebElement elementMap = encontarElementoEnXmlMapping(pagina, element);

			if (elementMap != null) {
				if (!isNullOrEmpty(elementMap.getById())) {
					return wait.until(ExpectedConditions.visibilityOf((this.driver.findElement(By.id(elementMap.getById())))));
				}
				if (!isNullOrEmpty(elementMap.getName())) {
					return wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.name(elementMap.getName()))));
				}
				if (!isNullOrEmpty(elementMap.getCssSelector())) {
					return wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.cssSelector(elementMap.getCssSelector()))));
				}
				if (!isNullOrEmpty(elementMap.getxPath())) {
					return wait.until(ExpectedConditions.visibilityOf(this.driver.findElement(By.xpath(elementMap.getxPath()))));
				}
			}

		}

		catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public boolean isNullOrEmpty(String str) {
		return str == null || str.equals("") || str.equals(" ");
	}

	private static MapeoWebElement encontarElementoEnXmlMapping(String page, String name) {

		MapeoWebElement elementMapping = null;
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(MapeoListaWebElements.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			File mappingFile = new File("src/main/resources/xmlMapping/" + page + ".xml");

			MapeoListaWebElements elementsList = (MapeoListaWebElements) jaxbUnmarshaller.unmarshal(mappingFile);

			for (MapeoWebElement wem : elementsList.getWebElements()) {
				if (wem.getElement().equals(name)) {
					elementMapping = wem;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return elementMapping;
	}

}
