package aplicacion.estructuraMapeo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "webElements")
@XmlAccessorType(XmlAccessType.FIELD)
public class MapeoListaWebElements {

	@XmlElement(name = "webElement")
	private List<MapeoWebElement> webElements = null;

	public List<MapeoWebElement> getWebElements() {
		return webElements;
	}

	public void setWebElements(List<MapeoWebElement> webElements) {
		this.webElements = webElements;
	}

}
