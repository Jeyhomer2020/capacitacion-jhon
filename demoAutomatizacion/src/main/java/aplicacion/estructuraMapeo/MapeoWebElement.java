package aplicacion.estructuraMapeo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "webElement")
@XmlAccessorType(XmlAccessType.NONE)
public class MapeoWebElement {
	private String element;
	private String name;
	private String xPath;
	private String cssSelector;
	private String byId;

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "XPath")
	public String getxPath() {
		return xPath;
	}

	public void setxPath(String xPath) {
		this.xPath = xPath;
	}

	@XmlElement(name = "CssSelector")
	public String getCssSelector() {
		return cssSelector;
	}

	public void setCssSelector(String cssSelector) {
		this.cssSelector = cssSelector;
	}

	@XmlElement(name = "ById")
	public String getById() {
		return byId;
	}

	public void setById(String byId) {
		this.byId = byId;
	}

	@XmlElement(name = "Element")
	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}
}
