package aplicacion.interfacesMapeo;

public interface IContactoSoftesting {
	
	 void diligenciarNombre(String nombre);
	 void diligenciarEmail(String email);
	 void diligenciarTipoConsulta(String tipoConsulta);
	 void diligenciarMensaje(String mensaje);
	 void cliquearBotonEnviar();
	 
	  
}
