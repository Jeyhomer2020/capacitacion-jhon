package dominio.modelos;

public class Contacto {
	
	private String nombre;
	private String email;
	private String emailIncorrecto;
	private String tipoConsulta;
	private String mensaje;
	
	

	public Contacto(String nombre, String email, String emailIncorrecto, String tipoConsulta, String mensaje) {
		this.nombre = nombre;
		this.email = email;
		this.tipoConsulta = tipoConsulta;
		this.mensaje = mensaje;
		this.emailIncorrecto = emailIncorrecto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getEmailIncorrecto() {
		return emailIncorrecto;
	}
	
	public void setEmailIncorrecto(String emailIncorrecto) {
		this.emailIncorrecto = emailIncorrecto;
	}
	
}
