package dominio.paginas;

import org.openqa.selenium.WebDriver;

public class PageBase {
	private final WebDriver driver;

	
	public PageBase(ShareWebDriver shareWebDriver) {
		driver = shareWebDriver.getDriver();
	}
	
	public void salir() {
		driver.quit();
	}
	
	public void iniciarParametrosAcceso() {
		// metodo de iniciacion de prarametros como tokens de acceso o credenciales adicionales 
	}
}
