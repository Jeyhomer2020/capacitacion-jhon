package dominio.paginas;

import org.openqa.selenium.JavascriptExecutor;

import dominio.modelos.Contacto;
import dominio.servicios.PaginaContactoSoftestingDom;

public class PaginaContactoSoftesting extends PageBase {

	private final ShareWebDriver driver;
	private final PaginaContactoSoftestingDom contactoDom;

	public PaginaContactoSoftesting(ShareWebDriver shareWebDriver, PaginaContactoSoftestingDom contactoDom) {
		super(shareWebDriver);
		driver = shareWebDriver;
		this.contactoDom = contactoDom;
	}

	public void diligenciarTodoContactenos(Contacto contacto) {
		this.contactoDom.diligenciarNombre(contacto.getNombre());
		this.contactoDom.diligenciarEmail(contacto.getEmail());
		this.contactoDom.diligenciarTipoConsulta(contacto.getTipoConsulta());
		this.contactoDom.diligenciarMensaje(contacto.getMensaje());
		this.contactoDom.cliquearBotonEnviar();
	}

	public void diligenciarNombreContactenos(String nombreContacto) {
		this.contactoDom.diligenciarNombre(nombreContacto);

	}

	public void diligenciarEmailContactenos(String email) {
		this.contactoDom.diligenciarEmail(email);

	}

	public void diligenciarTipoConsultaContactenos(String tipoConsulta) {
		this.contactoDom.diligenciarTipoConsulta(tipoConsulta);

	}

	public void diligenciarMensajeContactenos(String mensaje) {
		this.contactoDom.diligenciarMensaje(mensaje);

	}

	public void ir() {
		driver.ir("http://softesting.com/Test/contacto2/");
		JavascriptExecutor js = (JavascriptExecutor) driver.getDriver();
        js.executeScript("window.scrollBy(0,1000)");

	}

	public void validarTitulo() {
		this.contactoDom.validarTituloContacto();
	}

	public void validarCampoNombre() {
		this.contactoDom.validarCampoNombre();
	}

	public void validarCampoEmail() {
		this.contactoDom.validarCampoEmail();
	}

	public void validarCampoTipoConsulta() {
		this.contactoDom.validarCampoTipoDeConsulta();

	}

	public void validarCampoMensaje() {
		this.contactoDom.validarCampoMensaje();

	}

	public void cliquearBotonEnviar() {
		this.contactoDom.cliquearBotonEnviar();

	}

	public void validarCampoObligatorio() {
		this.contactoDom.validarCampoObligatorio();
	}

	public void validarCampoCorreoValido() {
		this.contactoDom.validarCampCorreoValido();

	}

	public void validarAgradecimiento() {
		this.contactoDom.muchasGraciasPorPonerteEnContacto();

	}

	public void cerrarDriver() {
		this.contactoDom.cerrarDriver();
	}

	public void volverAlInicio() {
		this.contactoDom.ubicarTituloContacto();
	}

	public void ubicarBotonEnviar() {
		this.contactoDom.ubicarBotonEnviar();
	}

}
