package dominio.paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import infraestructura.controladores.ConfiguracionEjecucion;
import infraestructura.utilidades.Utilidades;

public class ShareWebDriver {

	private static WebDriver driver;
	private static Robot robot = null;

	public ShareWebDriver(){

		try {
			ShareWebDriver.robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		driver = ConfiguracionEjecucion.getDriver();

	}

	

	public WebDriver getDriver() {
		return ShareWebDriver.driver;
	}
	
	public void ir(String url) {
		driver.get(url);
		Utilidades.esperar(2);
	}

	public void scrollAElemento(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView({" + " behavior: 'auto', " + " block: 'center', "
				+ " inline: 'center' " + " } );", element);
	}
	

	public Robot getRobot() {
		return robot;
	}

}
