package dominio.paginas;


public class StepsBase {
	
	private PageBase pageBase;
	
	public StepsBase(PageBase pageBase) {
		this.setPageBase(pageBase);

	}

	public PageBase getPageBase() {
		return pageBase;
	}

	public void setPageBase(PageBase pageBase) {
		this.pageBase = pageBase;
	}
}
