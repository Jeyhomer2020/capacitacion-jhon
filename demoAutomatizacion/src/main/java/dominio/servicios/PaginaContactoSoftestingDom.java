package dominio.servicios;

import static org.junit.Assert.assertEquals;

import java.awt.event.KeyEvent;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import aplicacion.estructuraMapeo.BaseMapeoDom;
import aplicacion.interfacesMapeo.IContactoSoftesting;
import dominio.paginas.ShareWebDriver;
import infraestructura.utilidades.CapturaPantalla;
import infraestructura.utilidades.Utilidades;

public class PaginaContactoSoftestingDom extends BaseMapeoDom implements IContactoSoftesting {

	private final WebDriver driver;
	private final ShareWebDriver shareWebDriver;
	private static final String NOMBREPAGINA = "paginaContacto";

	public PaginaContactoSoftestingDom(ShareWebDriver driver) {
		super(driver.getDriver(), NOMBREPAGINA);
		this.shareWebDriver = driver;
		this.driver = driver.getDriver();
	}

	public void diligenciarNombre(String nombre) {
		shareWebDriver.scrollAElemento(this.getWebElement("inputNombre"));
		this.getWebElement("inputNombre").sendKeys(nombre);
		CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputNombre"));
	}

	public void diligenciarEmail(String email) {
		shareWebDriver.scrollAElemento(this.getWebElement("inputEmail"));
		this.getWebElement("inputEmail").sendKeys(email);
		CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputEmail"), false);
	}

	public void diligenciarTipoConsulta(String tipoConsulta) {
		shareWebDriver.scrollAElemento(this.getWebElement("selectTipoConsulta"));
		this.getWebElement("selectTipoConsulta").findElement(By.xpath("//option[. = '" + tipoConsulta + "']")).click();
		CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("selectTipoConsulta"));
	}

	public void diligenciarMensaje(String mensaje) {
		shareWebDriver.scrollAElemento(this.getWebElement("inputMensaje"));
		this.getWebElement("inputMensaje").sendKeys(mensaje);
		CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputMensaje"));
		Utilidades.esperar(2);
		shareWebDriver.getDriver().findElement(By.tagName("body")).click();
	}

	public void cliquearBotonEnviar() {

		shareWebDriver.scrollAElemento(this.getWebElement("labelTipoConsulta"));
		shareWebDriver.scrollAElemento(this.getWebElement("inputMensaje"));
		shareWebDriver.scrollAElemento(this.getWebElement("botonEnviar"));
		CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("botonEnviar"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", this.getWebElement("botonEnviar"));
	}

	public void validarTituloContacto() {
		try {
			shareWebDriver.scrollAElemento(this.getWebElement("tituloContacto"));
			assertEquals("Contacto � Softesting", driver.getTitle());
			Utilidades.esperar(1);
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("tituloContacto"), true);
		} catch (AssertionError a) {
			Assert.fail("El titulo de la pagina no coincide; \n" + a);
		}
	}

	public void validarCampoNombre() {
		try {
			shareWebDriver.scrollAElemento(this.getWebElement("labelNombre"));
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputNombre"));
			Assert.assertTrue(this.getWebElement("labelNombre").getText().contains("Nombres"));
		} catch (Exception a) {
			Assert.fail("Problema al validar campo nombre " + a);
		}
	}

	public void validarCampoEmail() {
		try {
			shareWebDriver.scrollAElemento(this.getWebElement("inputEmail"));
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputEmail"));
			Assert.assertTrue(this.getWebElement("labelEmail").getText().contains("Email"));
		} catch (Exception a) {
			Assert.fail("Problema al validar campo email" + a);
		}
	}

	public void validarCampoTipoDeConsulta() {
		try {
			shareWebDriver.scrollAElemento(this.getWebElement("selectTipoConsulta"));
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("selectTipoConsulta"));
			Assert.assertTrue(this.getWebElement("labelTipoConsulta").getText().contains("Tipo de consulta"));
		} catch (Exception a) {
			Assert.fail("Problema al validar tipo de consulta");
		}
	}

	public void validarCampoMensaje() {
		try {
			shareWebDriver.scrollAElemento(this.getWebElement("inputMensaje"));
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("inputMensaje"));
			Assert.assertTrue(this.getWebElement("labelMensaje").getText().trim().contains("Mensaje"));
		} catch (Exception a) {
			Assert.fail("Problema al validar campo mensaje");
		}
	}

	public void muchasGraciasPorPonerteEnContacto() {
		try {
			CapturaPantalla.capturarPantallaElementoResaltado(driver, this.getWebElement("labelGracias"), true);

		} catch (Exception a) {
			Assert.fail("No se pudo encontrar el campo " + a);
		}
	}

	public void validarCampoObligatorio() {
		try {
			CapturaPantalla.capturarPantalla(true);
		} catch (Exception a) {
			Assert.fail("No ha enviado datos");
		}
	}

	public void validarCampCorreoValido() {
		try {
			WebElement element = driver
					.findElement(By.xpath("//*[@id=\"nf-form-1-cont\"]/div/div[4]/form/div/div[1]/nf-section/div"));
			shareWebDriver.scrollAElemento(element);
			CapturaPantalla.capturarPantalla();
		} catch (org.openqa.selenium.NoSuchElementException a) {
			Assert.fail("No se puede diligenciar el campoo");
		}
	}

	public void cerrarDriver() {
		driver.close();
	}

	public void ubicarBotonEnviar() {
		shareWebDriver.getRobot().mouseWheel(-5);
		shareWebDriver.getRobot().mouseWheel(2);
		shareWebDriver.scrollAElemento(this.getWebElement("tituloContacto"));
		shareWebDriver.getRobot().mouseWheel(2);
		shareWebDriver.scrollAElemento(this.getWebElement("labelNombre"));
		shareWebDriver.scrollAElemento(this.getWebElement("labelEmail"));
		shareWebDriver.scrollAElemento(this.getWebElement("labelTipoConsulta"));
		shareWebDriver.scrollAElemento(this.getWebElement("inputMensaje"));
		shareWebDriver.getRobot().mouseWheel(-1);
		shareWebDriver.scrollAElemento(this.getWebElement("botonEnviar"));

	}

	public void ubicarTituloContacto() {
		Utilidades.esperar(2);
		try {
			shareWebDriver.getDriver().findElement(By.tagName("body")).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		shareWebDriver.getRobot().keyPress(KeyEvent.VK_HOME);
		shareWebDriver.getRobot().keyRelease(KeyEvent.VK_HOME);
		shareWebDriver.getRobot().keyPress(KeyEvent.VK_DOWN);
		shareWebDriver.getRobot().keyRelease(KeyEvent.VK_DOWN);
		shareWebDriver.scrollAElemento(this.getWebElement("tituloContacto"));
		shareWebDriver.getRobot().keyPress(KeyEvent.VK_DOWN);
		shareWebDriver.getRobot().keyRelease(KeyEvent.VK_DOWN);

	}

}
