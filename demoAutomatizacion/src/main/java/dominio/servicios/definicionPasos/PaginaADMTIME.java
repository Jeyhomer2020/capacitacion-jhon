package dominio.servicios.definicionPasos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import dominio.paginas.PageBase;
import dominio.paginas.ShareWebDriver;
import dominio.paginas.StepsBase;
import infraestructura.utilidades.CapturaPantalla;

public class PaginaADMTIME extends StepsBase{

	private final ShareWebDriver driver;

	public PaginaADMTIME(PageBase pageBase) {
		super(pageBase);
		this.driver = new ShareWebDriver();
	}

	@Dado("^que el usuario ingresa a la pagina de ADMTIME$")
	public void que_el_usuario_ingresa_a_la_pagina_de_ADMTIME() throws Throwable {
	    driver.getDriver().get("https://138.68.40.204/admtime/vistas/login.html");
	    CapturaPantalla.capturarPantalla();
	}

	@Cuando("^el usuario ingresa su usuario$")
	public void el_usuario_ingresa_su_usuario() throws Throwable {
	    WebElement inputUsuario = driver.getDriver().findElement(By.id("logina"));
	    inputUsuario.sendKeys("USUARIO");
	    CapturaPantalla.capturarPantalla();

	}

	@Cuando("^tambien ingresa su contraseña$")
	public void tambien_ingresa_su_contraseña() throws Throwable {
		WebElement inputPass = driver.getDriver().findElement(By.id("clavea"));
	    inputPass.sendKeys("CONTRASEÑA");
	    CapturaPantalla.capturarPantalla();
	    
	    WebElement inputUsuario = driver.getDriver().findElement(By.cssSelector("#frmAcceso > div.row > div.col-xs-4 > button"));
	    CapturaPantalla.capturarPantalla();
	    inputUsuario.click();
	    
	}

	@Entonces("^el sistema muestra la pagina principal$")
	public void el_sistema_muestra_la_pagina_principal() throws Throwable {
	    CapturaPantalla.capturarPantalla();
	}
}
