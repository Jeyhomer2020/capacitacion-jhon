package dominio.servicios.definicionPasos;

import dominio.modelos.Contacto;
import dominio.paginas.PaginaContactoSoftesting;
import dominio.paginas.StepsBase;
import infraestructura.utilidades.CapturaPantalla;

import cucumber.api.java.After;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;;

public class PaginaContactoSteps extends StepsBase {

	private PaginaContactoSoftesting contactenosPage;
	private Contacto contactoCompleto;

	/* pagina de cntactenos */

	public PaginaContactoSteps(PaginaContactoSoftesting paginaContactenos) {
		super(paginaContactenos);
		this.contactenosPage = paginaContactenos;
		this.contactoCompleto = new Contacto("Prueba Automatizacion ", "demo2@softesting.com", "demo2softesting.com",
				"Soporte", "Esta es una demostracion de autmatizacion");
	}

	@Dado("^que el usuario ingresa a la pagina de contacto$")
	public void queElUsuarioIngresaALaPaginaDeContacto() {
		contactenosPage.ir();
		contactenosPage.volverAlInicio();
	}

	@Entonces("^Validar titulo de la pagina sea Contacto Softesting$")
	public void validarTituloDeLaPaginaSeaContactoSoftesting() {
		contactenosPage.validarTitulo();
	}

	@Entonces("^el usuario valida campo nombre$")
	public void elUsuarioValidaCampoNombre() {
		contactenosPage.validarCampoNombre();
	}

	@Entonces("^el usuario valida campo email$")
	public void elUsuarioValidaCampoEmail() {
		contactenosPage.validarCampoEmail();
	}

	@Entonces("^el usuario valida campo tipo de consulta$")
	public void elUsuarioValidaCampoTipoDeConsulta() {
		contactenosPage.validarCampoTipoConsulta();
	}

	@Entonces("^el usuario valida campo mensaje$")
	public void elUsuarioValidaCampoMensaje() {
		contactenosPage.validarCampoMensaje();
		CapturaPantalla.capturarPantalla(true);
	}
	

	@Dado("^el usuario da click en el boton enviar$")
	public void elUsuarioDaClickEnEnviar() {
		contactenosPage.cliquearBotonEnviar();
	}

	@Entonces("^Validar mensaje en rojo Esto es un campo obligatorio$")
	public void validarMensajeEnRojoEstoEsUnCampoObligatorio() {
		contactenosPage.validarCampoObligatorio();
	}

	@Dado("^El usuario llena campo nombre$")
	public void elUsuarioLlenaCampoNombre()  {
		contactenosPage.diligenciarNombreContactenos(contactoCompleto.getNombre());
	}

	@Dado("^el usuario llena campo email con una direccion de correo no valido$")
	public void elUsuarioLlenaCampoEmailConUnaDireccionDeCorreoNoValido()  {
		contactenosPage.diligenciarEmailContactenos(contactoCompleto.getEmailIncorrecto());
	}

	@Dado("^el usuario llena campo mensaje$")
	public void elUsuarioLlenaCampoMensaje()  {
		contactenosPage.diligenciarMensajeContactenos(contactoCompleto.getMensaje());
	}

	@Entonces("^validar mensaje en rojo Introduce una direccion de correo electronico valida$")
	public void validarCampoEnRojoIntroduceUnaDireccionDeCorreoElectronicoValida()  {
		contactenosPage.validarCampoCorreoValido();
	}

	@Dado("^el usuario llena campo email con una direccion de correo valido$")
	public void elUsuarioLlenaCampoEmailConUnaDireccionDeCorreoValido()  {
		contactenosPage.diligenciarEmailContactenos(contactoCompleto.getEmail());
	}

	@Entonces("^validar mensaje Muchas gracias por ponerte en contacto con nosotros$")
	public void validarCampoMuchasGraciasPorPonerteEnContactoConNosotros()  {
		contactenosPage.validarAgradecimiento();
	}

	@Cuando("^El usuario va al boton enviar$")
	public void elUsuarioVaAlBotonEnviar()  {
		contactenosPage.ubicarBotonEnviar();
	}
	

}
