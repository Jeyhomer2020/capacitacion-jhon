package infraestructura.controladores;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

import infraestructura.utilidades.CapturaPantalla;

public final class ConfiguracionEjecucion {

	///// CONSTANTES////////
	public static final String RUTA_ARCHIVO_CONFIG = "\\src\\main\\java\\config\\config.properties";
	public static final String RUTA_PAQUETE = "dominio";
	public static final String RUTA_FORMATTER = "com.cucumber.listener.ExtentCucumberFormatter:";
	public static final String RUTA_FEATURE = "src/main/resources/features";
	public static final String MENSAJE_OPCION_DOS = "Para ejecutar un caso favor seleccione la opcion 1 , y seleccione el caso que desea correr.";
	public static final String APLICACION = "PAGINA_SOFTESTING";
	public static final String RUTA_REPORTES_JSON = "C:\\ReportesJson";

	//////////////////////

	private static String ambiente = "PRODUCCION";
	private static String funcionalidad = "PAGINA_CONTACTENOS";
	private static int caso;
	private static String ubicacionReporte;
	private static String usuario;
	private static String password;
	private static String path;
	private static String browser = "";
	private static String nombreReporte;
	private static String numeroRadicacion;
	private static String sexoCliente;
	private static String fechaIngreso;
	private static String fechaRetiro;
	private static String documentoCliente;
	private static String documentoTrajador;
	private static boolean opcion1;
	private static boolean opcion2;
	private static String pathcontador;
	private static boolean estadoRegistroClientes = false;
	private static String ambienteRegistroClientes;
	private static boolean estadoEdge = false;
	private static String ip;
	private static String casosTexto;
	private static String periodoaValidar = "";
	private static Date validacion = new Date();
	private static int ncasos;
	private static String path2;
	private static String idProgramacion;

	private static WebDriver driver;

	public static void establecerRutasReporte() {

		path2 = getPath() + "\\" + getNombreReporte() + "\\" + getNombreReporte() + "_" + "reporte_y_evicendias\\";

		path = getPath() + "\\" + getNombreReporte();

	}

	public void crearCarpetaNombreReporte() {
		File newFolder = new File(getPath() + "\\" + getNombreReporte());
		newFolder.mkdirs();
	}

	private ConfiguracionEjecucion() {
		DateFormat formatoFechaHora = new SimpleDateFormat("yyyMMdd");
		String fechaActual = formatoFechaHora.format(new Date());
		setNombreReporte("PRUEBAS" + fechaActual);
	}

	public static WebDriver selectDriver() {

		switch (ConfiguracionEjecucion.browser) {
		case "ie":
			iniciarDriverInternetExplorer();
			break;
		case "firefox":
			iniciarDriverFirefox();
			break;
		case "chrome":
			iniciarDriverChrome();
			break;
		default:
			iniciarDriverChrome();
		}
		return driver;

	}

	private static void iniciarDriverFirefox() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAcceptUntrustedCertificates(true);				
		
		FirefoxOptions options = new FirefoxOptions();
		options.setProfile(profile);
		System.setProperty("webdriver.gecko.driver", "src/main/resources/drivers/geckodriver.exe");
		
		driver = new FirefoxDriver(options);
		driver.manage().window().maximize();
		
	}

	private static void iniciarDriverChrome() {
		ChromeOptions options = new ChromeOptions();
		 options.addArguments("--ignore-ssl-errors=yes");
		 options.addArguments("--ignore-certificate-errors"); 
		
		System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
	}

	private static void selectDriverInternetExplorer() {

		String driverTest = "src/main/resources/drivers/IEDriverServer.exe";
		String driverPrepro = "src/main/resources/drivers/IEDriverServer.exe";
		String driver = "";

		switch (System.getProperty("env")) {
		case "test":
			driver = driverTest;
			break;
		case "qa":
			driver = driverTest;

			break;
		case "prepro":
			driver = driverPrepro;
			break;
		default:
			driver = driverTest;
			break;
		}
		System.setProperty("webdriver.ie.driver", driver);
	}

	private static void iniciarDriverInternetExplorer() {
		try {
			ArrayList<String> output = new ArrayList<String>();
			Process p;

			p = Runtime.getRuntime().exec("reg query \"HKLM\\Software\\Microsoft\\Internet Explorer\" /v svcVersion");

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()), 8 * 1024);
			new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String s = null;
			while ((s = stdInput.readLine()) != null)
				output.add(s);
			String internetExplorerValue = (output.get(2));
			String version = internetExplorerValue.trim().split("   ")[2];
			String sSubCadena = version.substring(0, 3);

			if (sSubCadena.trim().equalsIgnoreCase("10")) {
				System.out.println("INTERNET EXPLORER 10");
				System.setProperty("webdriver.ie.driver", "src/main/resources/driversBrowser/IEDriverServerRR.exe");
			} else if (sSubCadena.trim().equalsIgnoreCase("11")) {
				selectDriverInternetExplorer();
			} else {
				System.setProperty("webdriver.ie.driver", "src/main/resources/drivers/IEDriverServerold.exe");
			}

			InternetExplorerOptions options = new InternetExplorerOptions();
			options.introduceFlakinessByIgnoringSecurityDomains();
			options.setCapability("ignoreZoomSetting", true);
			options.setCapability("requireWindowFocus", true);
			options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			options.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			driver = new InternetExplorerDriver(options);
			driver.manage().window().maximize();
			driver = new InternetExplorerDriver(options);
			driver.manage().window().maximize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void establecerParametrosAdicionales(String[] args) {

		for (int i = 4; i < args.length; i++) {
			if (args[i].charAt(0) == '-') {
				asignarParametro(args[i], args[i + 1]);
				i++;
			}
		}
	}

	public static void asignarParametro(String nombreParametro, String valorParametro) {
		// TODO Agregar los parametros correspondientes
		switch (nombreParametro.toUpperCase()) {
		case "-USUARIO":
			ConfiguracionEjecucion.setUsuario(valorParametro);
			System.out.println("usuario asignado");
			break;
		case "-PASSWORD":
			ConfiguracionEjecucion.setPassword(valorParametro);
			System.out.println("password asignado");
			break;
		}
	}

	private static final ConfiguracionEjecucion instance = new ConfiguracionEjecucion();

	public static ConfiguracionEjecucion getInstance() {
		return instance;
	}

	public static String getFuncionalidad() {
		return funcionalidad;
	}

	public static void setFuncionalidad(String funcionalidad) {
		ConfiguracionEjecucion.funcionalidad = funcionalidad;
	}

	public String getReport() {
		return ubicacionReporte + "/" + nombreReporte;
	}

	public static int getCaso() {
		return caso;
	}

	public static void setCaso(int caso) {
		ConfiguracionEjecucion.caso = caso;
	}

	public static String getUbicacionReporte() {
		return ubicacionReporte;
	}

	public static void setUbicacionReporte(String ubicacionReporte) {
		ConfiguracionEjecucion.ubicacionReporte = ubicacionReporte;
	}

	public static String getUsuario() {
		return usuario;
	}

	public static void setUsuario(String usuario) {
		ConfiguracionEjecucion.usuario = usuario;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		ConfiguracionEjecucion.password = password;
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		ConfiguracionEjecucion.path = path;
	}

	public static String getBrowser() {
		return browser;
	}

	public static void setBrowser(String browser) {
		ConfiguracionEjecucion.browser = browser;
	}

	public static String getNombreReporte() {
		return nombreReporte;
	}

	public static void setNombreReporte(String nombreReporte) {
		ConfiguracionEjecucion.nombreReporte = nombreReporte;
	}

	public static String getNumeroRadicacion() {
		return numeroRadicacion;
	}

	public static void setNumeroRadicacion(String numeroRadicacion) {
		ConfiguracionEjecucion.numeroRadicacion = numeroRadicacion;
	}

	public static String getSexoCliente() {
		return sexoCliente;
	}

	public static void setSexoCliente(String sexoCliente) {
		ConfiguracionEjecucion.sexoCliente = sexoCliente;
	}

	public static String getFechaIngreso() {
		return fechaIngreso;
	}

	public static void setFechaIngreso(String fechaIngreso) {
		ConfiguracionEjecucion.fechaIngreso = fechaIngreso;
	}

	public static String getFechaRetiro() {
		return fechaRetiro;
	}

	public static void setFechaRetiro(String fechaRetiro) {
		ConfiguracionEjecucion.fechaRetiro = fechaRetiro;
	}

	public static String getDocumentoCliente() {
		return documentoCliente;
	}

	public static void setDocumentoCliente(String documentoCliente) {
		ConfiguracionEjecucion.documentoCliente = documentoCliente;
	}

	public static String getDocumentoTrajador() {
		return documentoTrajador;
	}

	public static void setDocumentoTrajador(String documentoTrajador) {
		ConfiguracionEjecucion.documentoTrajador = documentoTrajador;
	}

	public static boolean isOpcion1() {
		return opcion1;
	}

	public static void setOpcion1(boolean opcion1) {
		ConfiguracionEjecucion.opcion1 = opcion1;
	}

	public static boolean isOpcion2() {
		return opcion2;
	}

	public static void setOpcion2(boolean opcion2) {
		ConfiguracionEjecucion.opcion2 = opcion2;
	}

	public static String getPathcontador() {
		return pathcontador;
	}

	public static void setPathcontador(String pathcontador) {
		ConfiguracionEjecucion.pathcontador = pathcontador;
	}

	public static boolean isEstadoRegistroClientes() {
		return estadoRegistroClientes;
	}

	public static void setEstadoRegistroClientes(boolean estadoRegistroClientes) {
		ConfiguracionEjecucion.estadoRegistroClientes = estadoRegistroClientes;
	}

	public static String getAmbiente() {
		return ambiente;
	}

	public static void setAmbiente(String ambiente) {
		ConfiguracionEjecucion.ambiente = ambiente;
	}

	public static String getAmbienteRegistroClientes() {
		return ambienteRegistroClientes;
	}

	public static void setAmbienteRegistroClientes(String ambienteRegistroClientes) {
		ConfiguracionEjecucion.ambienteRegistroClientes = ambienteRegistroClientes;
	}

	public static boolean isEstadoEdge() {
		return estadoEdge;
	}

	public static void setEstadoEdge(boolean estadoEdge) {
		ConfiguracionEjecucion.estadoEdge = estadoEdge;
	}

	public static String getIp() {
		return ip;
	}

	public static void setIp(String ip) {
		ConfiguracionEjecucion.ip = ip;
	}

	public static String getCasosTexto() {
		return casosTexto;
	}

	public static void setCasosTexto(String casosTexto) {
		ConfiguracionEjecucion.casosTexto = casosTexto;
	}

	public static String getPeriodoaValidar() {
		return periodoaValidar;
	}

	public static void setPeriodoaValidar(String periodoaValidar) {
		ConfiguracionEjecucion.periodoaValidar = periodoaValidar;
	}

	public static Date getValidacion() {
		return validacion;
	}

	public static void setValidacion(Date validacion) {
		ConfiguracionEjecucion.validacion = validacion;
	}

	public static String getPath2() {
		return path2;
	}

	public static void setPath2(String path2) {
		ConfiguracionEjecucion.path2 = path2;
	}

	public static int getNcasos() {
		return ncasos;
	}

	public static void setNcasos(int ncasos) {
		ConfiguracionEjecucion.ncasos = ncasos;
	}

	public static String getRutaPaquete() {
		return RUTA_PAQUETE;
	}

	public static String getRutaFormatter() {
		return RUTA_FORMATTER;
	}

	public static String getRutaFeature() {
		return RUTA_FEATURE;
	}

	public static String getMensajeOpcionDos() {
		return MENSAJE_OPCION_DOS;
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static String getIdProgramacion() {
		return idProgramacion;
	}

	public static void setIdProgramacion(String idProgramacion) {
		ConfiguracionEjecucion.idProgramacion = idProgramacion;
	}
}