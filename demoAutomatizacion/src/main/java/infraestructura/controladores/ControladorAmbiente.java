package infraestructura.controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ui.grafica.Ambiente;

public class ControladorAmbiente {
	
	private Ambiente editarAmbiente;
	
	public ControladorAmbiente () {		
		editarAmbiente= new Ambiente();				
	}
	
	public void index() {
		editarAmbiente.setBtnEnviarAction(new EventoEditar());	
		editarAmbiente.setBtnCancelarAction(new EventoCancelar());
		editarAmbiente.setVisible(true);		
	}
	
	private class EventoEditar implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			// accion de evento para boton enviar de la edicion de ambiente 
		}		
	}
	
	private class EventoCancelar implements ActionListener{
		public void actionPerformed(ActionEvent e) {			
			editarAmbiente.dispose();
		}		
	}
	
	
}
