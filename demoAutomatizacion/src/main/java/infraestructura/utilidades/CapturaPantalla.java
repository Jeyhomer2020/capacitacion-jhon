package infraestructura.utilidades;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import infraestructura.controladores.ConfiguracionEjecucion;

public class CapturaPantalla {

	private CapturaPantalla() {
	}

	static int cont = 0;
	static Random aleatorio = new Random();
	private static final String B64 = "data:image/png;base64,";

	public static String fechaSistema() {

		Calendar c = Calendar.getInstance();
		int dia = c.get(Calendar.DATE);
		int mes = c.get(Calendar.MONTH);
		int anio = c.get(Calendar.YEAR);

		return String.valueOf(
				anio + "-" + (((mes + 1) < 10 ? "0" + (mes + 1) : (mes + 1)) + "-" + (dia < 10 ? "0" + dia : dia)));

	}

	public static String horaSistema() {

		Calendar c = Calendar.getInstance();
		int hr = c.get(Calendar.HOUR);
		int min = c.get(Calendar.MINUTE);

		return (hr + "." + min);
	}

	public static String captureScreen() {
		String imageBase64 = "";
		try {
			BufferedImage image = new Robot()
					.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			byte[] imageBytes = baos.toByteArray();
			String temp = new String(Base64.getEncoder().encode(imageBytes));
			imageBase64 = B64 + temp;
			baos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imageBase64;
	}

	public static String capture(WebDriver driver) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File dest = new File(".\\evid\\" + File.separator + ConfiguracionEjecucion.getNombreReporte() + File.separator
				+ System.currentTimeMillis() + ".png");
		String errflpath = dest.getAbsolutePath();
		FileUtils.copyFile(scrFile, dest);
		return errflpath.replace(ConfiguracionEjecucion.getPath(), "..");
	}

	public static void capturarPantalla(boolean lastStep) {
		// Captura de pantalla
		String captura = CapturaPantalla.captureScreen();
		try {
			agregarEvidencia(captura, lastStep);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void capturarPantalla() {
		// Captura de pantalla
		String captura = CapturaPantalla.captureScreen();
		try {
			agregarEvidencia(captura, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void capturarPantallaElementoResaltado(WebDriver driver, WebElement elemento, boolean lastStep) {
		// Captura de pantalla
		String captura = takeScreenShotElement(driver, elemento);
		try {
			agregarEvidencia(captura, lastStep);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void capturarPantallaElementoResaltado(WebDriver driver, WebElement elemento) {
		// Captura de pantalla
		String captura = takeScreenShotElement(driver, elemento);
		try {
			agregarEvidencia(captura, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void capturarPantallaResaltada(WebDriver driver, int x, int y, int w, int h,
			boolean lastStep) {

		System.out.println(x + " " + y + " " + w + " " + h);
		String captura = takeScreenShotXYWH(driver, x, y, w, h);
		try {
			agregarEvidencia(captura, lastStep);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String takeScreenShot(String imageName, WebDriver driver, WebElement elemento) {

		String path = "";

		path = ConfiguracionEjecucion.getPath() + "\\" + imageName + System.currentTimeMillis() + ".png";
		try {

			int x = elemento.getLocation().getX();

			int y = elemento.getLocation().getY();

			int width = elemento.getSize().getWidth();

			int height = elemento.getSize().getHeight();

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			BufferedImage image = ImageIO.read(scrFile);

			Graphics2D graphics = image.createGraphics();

			graphics.setFont(graphics.getFont().deriveFont(30f));

			// The colour for the rectangle which is to be drawn around the
			// element

			graphics.setColor(Color.RED);

			// Thickness of each side of the rectangle

			graphics.setStroke(new BasicStroke(4.0f));

			graphics.drawRect(x, y, width, height);

			ImageIO.write(image, "png", new File(path));

		} catch (IOException e) {

			// Impresion de Excepciones

			e.printStackTrace();
		}

		return path.replace(ConfiguracionEjecucion.getPath(), "..");

	}

	public static String takeScreenShotXYWH(WebDriver driver, int x, int y, int width, int height) {

		String imageBase64 = "";
		try {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			BufferedImage image = ImageIO.read(scrFile);

			Graphics2D graphics = image.createGraphics();

			graphics.setFont(graphics.getFont().deriveFont(30f));

			// The colour for the rectangle which is to be drawn around the
			// element

			graphics.setColor(Color.RED);

			// Thickness of each side of the rectangle

			graphics.setStroke(new BasicStroke(4.0f));

			graphics.drawRect(x, y, width, height);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			byte[] imageBytes = baos.toByteArray();
			String temp = new String(Base64.getEncoder().encode(imageBytes));
			imageBase64 = B64 + temp;
			baos.close();

		} catch (IOException e) {

			// Impresion de Excepciones

			e.printStackTrace();
		}

		return imageBase64;

	}

	public static String takeScreenShotElement(WebDriver driver, WebElement elemento) {
		String old = "";
		try {
			old = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].style.border", elemento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", elemento);
		String imageBase64 = null;
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			BufferedImage image = ImageIO.read(scrFile);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			byte[] imageBytes = baos.toByteArray();
			String temp = new String(Base64.getEncoder().encode(imageBytes));
			imageBase64 = B64 + temp;
			baos.close();
		} catch (IOException e) {
			// Impresion de Excepciones
			e.printStackTrace();
		}

		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].style.border='" + old + "'", elemento);
		} catch (Exception e) {
			((JavascriptExecutor) driver).executeScript("arguments[0].style.border='0px'", elemento);
		}

		return imageBase64;
	}

	private static void agregarEvidencia(String imagen, boolean ultimoPaso) {
		try {
			if (ultimoPaso) {
				com.cucumber.listener.Reporter.addStepLog(getHTML(imagen, ultimoPaso));
			} else {

				com.cucumber.listener.Reporter.addStepLog(getHTML(imagen, false));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getHTML(String imagen, boolean active) {
		return "<ul class='collapsible'>" + "<li style='border:none' >"
				+ "<div class='collapsible-header right' style='margin-top: -20px;padding:0;'>"
				+ (active ? "" : "<a href='#'><i class='material-icons'>panorama</i></a>") + "</div>" + "<div class='"
				+ (active ? "" : "collapsible-body") + "' style='padding:0;border:none;'>"
				+ "<img style='width:100%;' src=" + imagen + " />" + "</div>" + "</li>" + "</ul>";	 
	}

}
