package infraestructura.utilidades;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import infraestructura.controladores.ConfiguracionEjecucion;

public class MapeadorReporteJson {

	private int pasosPasados = 0;
	private int pasosFallidos = 0;
	private int casosPasados = 0;
	private int casosFallidos = 0;
	private int totalCasos = 0;
	
	private JSONParser jsonParser = new JSONParser();

	ArrayList<File> reportesJson = new ArrayList();

	public void listarReportesJson() {
		File dir = new File(ConfiguracionEjecucion.RUTA_REPORTES_JSON);
		File[] files = dir.listFiles();
		for (File file : files) {
			reportesJson.add(file);
		}
		ordenarReportes();
	}

	private void ordenarReportes() {
		reportesJson.sort(new Comparator<File>() {

			public int compare(File o1, File o2) {
				long f1 = o1.lastModified();
				long f2 = o2.lastModified();
				return (int) (f1 > f2 ? f1 : f2);
			}
		});

	}

	public void lecturaReporteJson() {
		try (FileReader reader = new FileReader(reportesJson.get(2))) {
			Object obj = jsonParser.parse(reader);
			JSONArray reportData = (JSONArray) obj;
			for (int i = 0; i < reportData.size(); i++) {

				JSONArray elements = (JSONArray) ((JSONObject) reportData.get(i)).get("elements");

				totalCasos++;
				pasosFallidos = 0;
				setPasosPasados(0);
				for (int j = 0; j < elements.size(); j++) {
					JSONArray steps = ((JSONArray) ((JSONObject) elements.get(0)).get("steps"));
					for (int k = 0; k < steps.size(); k++) {
						if ((((JSONObject) ((JSONObject) steps.get(k)).get("result")).get("status")).toString()
								.equalsIgnoreCase("passed")) {
							setPasosPasados(getPasosPasados() + 1);
						} else {
							pasosFallidos++;
						}
					}
					if (pasosFallidos != 0) {
						casosFallidos++;
					} else {
						casosPasados++;
					}
				}
			}

			System.out.println(casosPasados + " " + casosFallidos + "  " + totalCasos);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getPasosPasados() {
		return pasosPasados;
	}

	public void setPasosPasados(int pasosPasados) {
		this.pasosPasados = pasosPasados;
	}

}
