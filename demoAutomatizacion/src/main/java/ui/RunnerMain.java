package ui;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aventstack.extentreports.ExtentReports;
import com.cucumber.listener.ExtentProperties;
import com.github.mkolisnyk.cucumber.reporting.CucumberDetailedResults;

import cucumber.api.cli.Main;
import infraestructura.controladores.ConfiguracionEjecucion;

public class RunnerMain {

	public void runner() {

		try {
			Thread hiloA = new Thread(new Hilo(), "hiloA");
			hiloA.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class Hilo extends Thread {

		public void run() {
			try {
				DateFormat formatoFechaHora = new SimpleDateFormat("yyyMMdd-HH.mm");
				String fechaHoraActual = formatoFechaHora.format(new Date());
				String nombreReporte = ConfiguracionEjecucion.getAmbiente() + "-" + ConfiguracionEjecucion.APLICACION
						+ "-" + ConfiguracionEjecucion.getFuncionalidad() + "-" + fechaHoraActual;

				ExtentReports extentReports = new ExtentReports();
				extentReports.setGherkinDialect("es");
				ExtentProperties extentProperties = ExtentProperties.INSTANCE;
				extentProperties.setProjectName("Reporte Pruebas automatizadas SICOM");

				extentProperties.setReportPath(ConfiguracionEjecucion.getUbicacionReporte() + "/"
						+ ConfiguracionEjecucion.getNombreReporte() + "/" + nombreReporte + ".html");

				// Creacion de las carpetas de evidencias y capturas
				System.out.println(ConfiguracionEjecucion.getPath());
				File newFolder2 = new File(ConfiguracionEjecucion.getPath());
				newFolder2.mkdirs();

				ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
				List<String> where = new ArrayList();
				String t = "";
				String tag = "@Caso";

				if (ConfiguracionEjecucion.getCasosTexto().equalsIgnoreCase("Todos")) {
					for (int i = 1; i < 6; i++) {
						where.add(tag + i + "");
					}
				} else if (ConfiguracionEjecucion.getCasosTexto() != null
						&& ConfiguracionEjecucion.getCasosTexto().length() > 0) {
					String casos = ConfiguracionEjecucion.getCasosTexto().replace("Caso", "");
					String[] txtNumeroCasos = casos.split(",");
					for (int i = 0; i < txtNumeroCasos.length; i++) {
						String casoRunner = txtNumeroCasos[i].trim();
						where.add(tag.concat(casoRunner));
					}
				} else if (!ConfiguracionEjecucion.getCasosTexto().equals("Todos")) {
					where.add(tag.concat("5"));
				}

				for (int i = 0; i < where.size(); i++) {
					t += ((i > 0) ? "," : "") + where.get(i);
				}

				// Run de los casos por medio de cucumber
				if (!t.equals("")) {
					String[] argv = new String[] { "-g", ConfiguracionEjecucion.RUTA_PAQUETE, "-p",
							ConfiguracionEjecucion.RUTA_FORMATTER, "-p",
							"json:C:\\ReportesJson\\" + nombreReporte + ".json", "-t", t,
							ConfiguracionEjecucion.RUTA_FEATURE };

					ConfiguracionEjecucion.selectDriver();
					Main.run(argv, contextClassLoader);

					// terminar driver
					ConfiguracionEjecucion.getDriver().close();
					ConfiguracionEjecucion.getDriver().quit();

					CucumberDetailedResults results = new CucumberDetailedResults();

					results.setOutputDirectory(ConfiguracionEjecucion.getUbicacionReporte() + "/"
							+ ConfiguracionEjecucion.getNombreReporte() + "/");
					results.setOutputName(nombreReporte);
					results.setSourceFile("C:\\ReportesJson\\" + nombreReporte + ".json");
					results.executeDetailedResultsReport(true);

					// cerrar procesos driver
					/*
					 * Process p1 = Runtime.getRuntime().exec(
					 * "src\\main\\java\\resources\\drivers\\CerrarProceso.bat"); p1.waitFor();
					 */
				}
			} catch (

			Exception e) {
				e.printStackTrace();
			}
		}
	}

}
