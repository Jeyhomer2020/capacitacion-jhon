package ui.consola;

import infraestructura.controladores.ConfiguracionEjecucion;
import ui.RunnerMain;

public class ControladorInicioConsola {

	public static void main(String[] args) {
		// parametros minimos de ejecucion desde consola
		//POR FAVOR NO CAMBIAR EL ORDEN, 
		//pues este est� adaptado a ASADE para las ejecucjones automaticas 
		ConfiguracionEjecucion.setCasosTexto(args[0]);
		ConfiguracionEjecucion.setBrowser(args[1]);
		ConfiguracionEjecucion.setPath(args[2]);
		ConfiguracionEjecucion.setUbicacionReporte(args[2]);
		ConfiguracionEjecucion.setNombreReporte(args[3]);
		ConfiguracionEjecucion.establecerRutasReporte();

		ConfiguracionEjecucion.establecerParametrosAdicionales(args);

		RunnerMain runnerMain = new RunnerMain();
		runnerMain.runner();
	}

}
