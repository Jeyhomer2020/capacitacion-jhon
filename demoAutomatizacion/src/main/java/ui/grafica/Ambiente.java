package ui.grafica;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Ambiente extends JDialog {
	
	private static final long serialVersionUID = 2L;
	private JTextField txtUrl;

	private JTextField txtDatabase;
	private JButton btnCancelar;
	private JButton btnEnviar;


	public Ambiente() {
		
		setBounds(0, 0, 450, 300);
		setLocationRelativeTo(null);
		setModal(true);		
		JPanel mHeader = new JPanel();
		JPanel mBody = new JPanel();
		JPanel mFooter = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(mHeader, GroupLayout.PREFERRED_SIZE, 433, GroupLayout.PREFERRED_SIZE)
						.addComponent(mFooter, GroupLayout.PREFERRED_SIZE, 434, GroupLayout.PREFERRED_SIZE)
						.addComponent(mBody, GroupLayout.PREFERRED_SIZE, 434, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(mHeader, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(mBody, GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(mFooter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		);
		mFooter.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		btnEnviar = new JButton("Enviar");
		mFooter.add(btnEnviar);
		
		btnCancelar = new JButton("Cancelar");
		mFooter.add(btnCancelar);
		
		JPanel fUrl = new JPanel();
		JPanel fDb = new JPanel();
		
		FlowLayout flowLayout2 = (FlowLayout) fDb.getLayout();
		flowLayout2.setAlignment(FlowLayout.LEADING);
		GroupLayout glMBody = new GroupLayout(mBody);
		glMBody.setHorizontalGroup(
			glMBody.createParallelGroup(Alignment.LEADING)
				.addGroup(glMBody.createSequentialGroup()
					.addGap(22)
					.addGroup(glMBody.createParallelGroup(Alignment.LEADING, false)
						.addComponent(fDb)
						.addComponent(fUrl))
					.addContainerGap())
		);
		glMBody.setVerticalGroup(
			glMBody.createParallelGroup(Alignment.LEADING)
				.addGroup(glMBody.createSequentialGroup()
					.addComponent(fUrl)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(fDb)
					.addContainerGap(23, Short.MAX_VALUE))
		);
		

		
		JLabel jlDatabase = new JLabel("Database");
		jlDatabase.setPreferredSize(new Dimension(100, 50));
		fDb.add(jlDatabase);
		
		txtDatabase = new JTextField();
		txtDatabase.setLocation(100, 5);
		fDb.add(txtDatabase);
		txtDatabase.setColumns(10);
		
	
		
		JLabel jlUrl = new JLabel("URL");
		jlUrl.setPreferredSize(new Dimension(100, 50));

		fUrl.add(jlUrl);
		
		txtUrl = new JTextField();
		txtUrl.setLocation(100, 5);
		fUrl.add(txtUrl);
		txtUrl.setColumns(10);
		
		
		mBody.setLayout(glMBody);
		mHeader.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel jlTituloAmbiente = new JLabel("Configuración Ambiente") ;
		mHeader.add(jlTituloAmbiente);
		getContentPane().setLayout(groupLayout);
	}
	
	public void setBtnEnviarAction(ActionListener a) {
		btnEnviar.addActionListener(a);
	}
	
	public void setBtnCancelarAction(ActionListener a) {		
		btnCancelar.addActionListener(a);
	}
}