package ui.grafica;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

import infraestructura.controladores.ConfiguracionEjecucion;
import infraestructura.controladores.ControladorAmbiente;
import ui.RunnerMain;


public class ControladorInicio {
	

	private Inicio vista;
	private RunnerMain main;
	public ControladorInicio() {
	
		this.vista = new Inicio();
		this.main = new RunnerMain();
	}
	
	
		public void index() {	
		vista.setVisible(true);
		vista.getbtnAbrirUbicacion().addActionListener(new EventoAbrirUbicacion());
		vista.getJcbCasos().addActionListener(new EventoCasos());
		vista.getJcbModulo().addActionListener(new EventoModulo());
		vista.getJcbSeleccionAmbiente().addActionListener(new EventoEnviar());
		vista.getJchPassword().addActionListener(new EventoPassword());
		vista.getBtnAceptar().addActionListener(new EventoRun());
		vista.getBtnEditar().addActionListener(new EventoEditar());
	}
	
	private class EventoAbrirUbicacion implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
	        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        int returnVal = chooser.showOpenDialog(null);
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File path = chooser.getSelectedFile();
                ConfiguracionEjecucion.setUbicacionReporte(path.getAbsolutePath());
	            vista.setUbicacionReporte(path.getAbsolutePath());
	        }			
		}
		
	}
	
	private class EventoCasos implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			ConfiguracionEjecucion.setCaso(vista.getJcbCasos().getSelectedIndex()+1);			
		}
		
	}
	private class EventoModulo implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			int i =  vista.getJcbModulo().getSelectedIndex();
			if(i == 0) {

				ConfiguracionEjecucion.setFuncionalidad("Modulo uno");

			}else if(i ==1) {
				
				ConfiguracionEjecucion.setFuncionalidad("Modulo dos");
		
			}else {			

				ConfiguracionEjecucion.setFuncionalidad("Modulo tres");						

			}			
		}
		
	}
	
	private class EventoEnviar implements ActionListener  {

		public void actionPerformed(ActionEvent e) {
			System.out.println("Entro editar entorno");
		}
		
	}
	
	private class EventoPassword implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			if (vista.getJchPassword().isSelected()) {
				vista.getJpsPassword().setEchoChar((char) 0);
	        } else {
	        	vista.getJpsPassword().setEchoChar('*');
	        }
			
		}
		
	}
	
	private class EventoRun implements ActionListener{

		public void actionPerformed(ActionEvent e) {			
			llenarEjecucion();			
			main.runner();
		}
		
	}
	
	private class EventoEditar implements ActionListener{

		public void actionPerformed(ActionEvent e) {			
			ControladorAmbiente controladorAmbiente = new ControladorAmbiente();
			controladorAmbiente.index();
			System.out.println("editar");
		}
		
	}
	
	@SuppressWarnings("deprecation")
	private void llenarEjecucion() {
		ConfiguracionEjecucion.setUsuario(vista.getTxtUsuario().getText());
		ConfiguracionEjecucion.setPassword(vista.getJpsPassword().getText());
		ConfiguracionEjecucion.setCasosTexto(establecerCasosSelecionados());
		ConfiguracionEjecucion.setPath(vista.getTxtUbicacionReporte().getText());
		ConfiguracionEjecucion.setNombreReporte(vista.getTxtNombreReporte().getText());
		ConfiguracionEjecucion.establecerRutasReporte();
		establecerNavegadorSelecionado();
	}


	private String establecerCasosSelecionados() {
		if (vista.getJrbMultiplesCasos().isSelected()) {
			return vista.getTxtMultiplesCasos().getText();
		}	
		return vista.getJcbCasos().getSelectedItem().toString();
	}


	private void establecerNavegadorSelecionado() {
		String navegador = vista.getRdbMozillaFirefox().isSelected()? "firefox":
			vista.getRdbGoogleChrome().isSelected()?"chrome":"";
		ConfiguracionEjecucion.setBrowser(navegador);
	}
}