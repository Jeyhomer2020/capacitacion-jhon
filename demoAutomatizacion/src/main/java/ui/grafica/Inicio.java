package ui.grafica;

import javax.swing.GroupLayout;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import infraestructura.controladores.ConfiguracionEjecucion;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Inicio extends JFrame {

	private static final long serialVersionUID = 1L;
	private static int ncasos = 5;

	private javax.swing.JButton btnAbrirUbicacion;
	private javax.swing.JButton btnAceptar;
	private javax.swing.JButton btnEditar;
	private javax.swing.ButtonGroup bgNavegadores;
	private javax.swing.JLabel jlTitulo;
	private javax.swing.JLabel jlSubTitulo;
	private javax.swing.JLabel jlSeleccionModulo;
	private javax.swing.JPanel jpConfiguracionReporte;
	private javax.swing.JPanel jpConfiguracionCasos;
	private javax.swing.JPanel jpSeleccionAmbiente;
	private javax.swing.JPanel jpCredencialesAcceso;
	private javax.swing.JComboBox<String> jcbCasos;
	private javax.swing.JComboBox<String> jcbModulo;
	private javax.swing.JComboBox<String> jcbSeleccionAmbiente;
	private javax.swing.JCheckBox jchPassword;
	private javax.swing.JPasswordField jpsPassword;
	private javax.swing.JLabel jlNombreReporte;
	private javax.swing.JLabel jlUbicacionReporte;
	private javax.swing.JLabel jlUsuario;
	private javax.swing.JLabel jlPassword;
	private javax.swing.JLabel jlVerificacionPassword;
	private javax.swing.JRadioButton rdbMozillaFirefox;
	private javax.swing.JRadioButton rdbGoogleChrome;
	private javax.swing.JTextField txtNombreReporte;
	private javax.swing.JTextField txtUbicacionReporte;
	private javax.swing.JTextField txtUsuario;
	private javax.swing.JMenuBar JMenuBar;
	private javax.swing.JMenuItem menuConfiguracion;
	private javax.swing.JMenu menuDeckPruebas;
	private javax.swing.JMenu menuDocumentacion;
	private javax.swing.JMenu mnManuales;
	private javax.swing.JMenuItem mnuItemAcerca;
	private javax.swing.JMenuItem mnuItemGuiaUsuario;
	private javax.swing.JMenuItem mnuItemPrestacionesEconomicas;
	private javax.swing.JMenu helpMenu;
	private JTextField txtMultiplesCasos;
	private JRadioButton jrbOpcionCasoUnico;
	private JRadioButton jrbMultiplesCasos;

	public Inicio() {

		componentes();
		ventana();
		menu();
	}

	private void ventana() {
		setTitle("SOFTESTING");
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setIconImage(getIconImagen());
	}

	public Image getIconImagen() {
		File file = new File("src/main/resources/imagenes/logo.png");
		return Toolkit.getDefaultToolkit().getImage(file.getPath());
	}

	private void menu() {

		JMenuBar = new javax.swing.JMenuBar();
		mnManuales = new javax.swing.JMenu();
		menuDeckPruebas = new javax.swing.JMenu();
		mnuItemPrestacionesEconomicas = new javax.swing.JMenuItem();
		helpMenu = new javax.swing.JMenu();
		menuDocumentacion = new javax.swing.JMenu();
		mnuItemGuiaUsuario = new javax.swing.JMenuItem();
		menuConfiguracion = new javax.swing.JMenuItem();
		mnuItemAcerca = new javax.swing.JMenuItem();

		mnManuales.setMnemonic('e');
		mnManuales.setText("Deck");
		mnManuales.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				mnManualesActionPerformed();
			}
		});

		menuDeckPruebas.setText("Deck de Pruebas  ");

		mnuItemPrestacionesEconomicas.setText("Demo");
		mnuItemPrestacionesEconomicas.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				mnuItemPrestacionesEconomicasActionPerformed();
			}
		});

		menuDeckPruebas.add(mnuItemPrestacionesEconomicas);

		mnManuales.add(menuDeckPruebas);

		JMenuBar.add(mnManuales);

		helpMenu.setMnemonic('h');
		helpMenu.setText("Ayuda");

		menuDocumentacion.setText("Documentación");

		mnuItemGuiaUsuario.setText("Guia Usuario");
		mnuItemGuiaUsuario.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				mnuItemGuiaUsuarioActionPerformed();
			}
		});
		menuDocumentacion.add(mnuItemGuiaUsuario);

		menuConfiguracion.setText("Manual Configuración ");
		menuConfiguracion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menuConfiguracionActionPerformed();
			}
		});
		menuDocumentacion.add(menuConfiguracion);

		helpMenu.add(menuDocumentacion);

		mnuItemAcerca.setMnemonic('a');
		mnuItemAcerca.setText("Acerca");
		mnuItemAcerca.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				mnuItemAcercaActionPerformed();
			}
		});

		helpMenu.add(mnuItemAcerca);

		JMenuBar.add(helpMenu);

		setJMenuBar(JMenuBar);

	}

	private void mnManualesActionPerformed() {
		// btn manuales
	}

	private void mnuItemGuiaUsuarioActionPerformed() {
		try {
			File path = new File("src/resources/Documentos/MANUAL_GUIA_DE_USUARIO.pdf");
			Desktop.getDesktop().open(path);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void menuConfiguracionActionPerformed() {
		try {
			File path = new File("src/resourcesDocumentos/MANUAL_CONFIGURACION.pdf");
			Desktop.getDesktop().open(path);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void mnuItemAcercaActionPerformed() {
		// vista acerca de
	}

	private void mnuItemPrestacionesEconomicasActionPerformed() {
		try {
			File path = new File("src/resourcesDocumentos/Deck.xlsx");
			Desktop.getDesktop().open(path);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void addItems() {
		ConfiguracionEjecucion.getInstance();
		String[] nCasos = new String[ConfiguracionEjecucion.getNcasos()];
		this.jcbCasos.removeAllItems();
		for (int i = 0; i < nCasos.length; i++) {
			nCasos[i] = "Caso" + (i + 1);
			this.jcbCasos.addItem("Caso" + (i + 1));
		}
	}

	private void encabezado() {

		jlTitulo = new javax.swing.JLabel("DEMO AUTOMATIZACION SOFTESTING", SwingConstants.CENTER);
		jlTitulo.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N

		jlSubTitulo = new javax.swing.JLabel("Prueba Automatización", SwingConstants.CENTER);
		jlSubTitulo.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N

	}

	private void credencialesAcceso() {

		jpCredencialesAcceso = new javax.swing.JPanel();
		jpCredencialesAcceso.setBorder(javax.swing.BorderFactory.createTitledBorder("Credenciales de acceso"));

		jlUsuario = new javax.swing.JLabel("Usuario:");
		jlPassword = new javax.swing.JLabel("Password:");
		txtUsuario = new javax.swing.JTextField("softesting");
		jpsPassword = new javax.swing.JPasswordField("Demo2020");
		jlVerificacionPassword = new javax.swing.JLabel();
		jchPassword = new javax.swing.JCheckBox();
		btnAceptar = new javax.swing.JButton("Aceptar");

		txtUsuario.setEnabled(false);
		jpsPassword.setEnabled(false);

		javax.swing.GroupLayout jpCredencialesAccesoLayout = new javax.swing.GroupLayout(jpCredencialesAcceso);
		jpCredencialesAcceso.setLayout(jpCredencialesAccesoLayout);
		jpCredencialesAccesoLayout.setHorizontalGroup(
				jpCredencialesAccesoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jpCredencialesAccesoLayout.createSequentialGroup().addContainerGap()
								.addGroup(jpCredencialesAccesoLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jlUsuario).addComponent(jlPassword))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(jpCredencialesAccesoLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
										.addComponent(jlVerificacionPassword, javax.swing.GroupLayout.Alignment.LEADING,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(jpsPassword, javax.swing.GroupLayout.Alignment.LEADING,
												javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
										.addComponent(txtUsuario))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(jchPassword).addGap(71, 71, 71)));
		jpCredencialesAccesoLayout.setVerticalGroup(jpCredencialesAccesoLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jpCredencialesAccesoLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(jpCredencialesAccesoLayout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(jlUsuario))
						.addGroup(jpCredencialesAccesoLayout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpCredencialesAccesoLayout
										.createSequentialGroup()
										.addComponent(jlVerificacionPassword, javax.swing.GroupLayout.PREFERRED_SIZE,
												28, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(10, 10, 10))
								.addGroup(jpCredencialesAccesoLayout.createSequentialGroup()
										.addGroup(jpCredencialesAccesoLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(jchPassword)
												.addGroup(jpCredencialesAccesoLayout
														.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jlPassword).addComponent(jpsPassword,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))) //
								))));

	}

	private void configuracionReporte() {

		jpConfiguracionReporte = new javax.swing.JPanel();
		jpConfiguracionReporte.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración del reporte"));

		jlNombreReporte = new javax.swing.JLabel("Nombre del reporte: ");
		jlUbicacionReporte = new javax.swing.JLabel("Ubicación del reporte:");
		txtNombreReporte = new javax.swing.JTextField();
		txtUbicacionReporte = new javax.swing.JTextField();
		btnAbrirUbicacion = new javax.swing.JButton();

		ConfiguracionEjecucion.getInstance();
		txtNombreReporte.setText(ConfiguracionEjecucion.getNombreReporte());

		btnAbrirUbicacion.setBackground(new java.awt.Color(204, 204, 255));
		btnAbrirUbicacion.setText("...");

		javax.swing.GroupLayout jpConfiguracionReporteLayout = new javax.swing.GroupLayout(jpConfiguracionReporte);
		jpConfiguracionReporte.setLayout(jpConfiguracionReporteLayout);
		jpConfiguracionReporteLayout.setHorizontalGroup(
				jpConfiguracionReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jpConfiguracionReporteLayout.createSequentialGroup()
								.addGroup(jpConfiguracionReporteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jlUbicacionReporte).addComponent(jlNombreReporte))
								.addGap(18, 18, 18)
								.addGroup(jpConfiguracionReporteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
										.addComponent(txtNombreReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 316,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGroup(jpConfiguracionReporteLayout.createSequentialGroup()
												.addComponent(txtUbicacionReporte)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(btnAbrirUbicacion, javax.swing.GroupLayout.PREFERRED_SIZE,
														72, javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addContainerGap(36, Short.MAX_VALUE)));
		jpConfiguracionReporteLayout.setVerticalGroup(jpConfiguracionReporteLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jpConfiguracionReporteLayout.createSequentialGroup().addContainerGap()
						.addGroup(jpConfiguracionReporteLayout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jlNombreReporte).addComponent(txtNombreReporte,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jpConfiguracionReporteLayout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jlUbicacionReporte).addComponent(btnAbrirUbicacion)
								.addComponent(txtUbicacionReporte, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(28, Short.MAX_VALUE)));
	}

	private void configuracionCasos() {

		jpConfiguracionCasos = new javax.swing.JPanel();
		jpConfiguracionCasos.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuracion de los casos"));

		jlSeleccionModulo = new javax.swing.JLabel("Selección de Moduló:");
		jcbModulo = new javax.swing.JComboBox<>();
		jcbCasos = new javax.swing.JComboBox<>();

		addItems();

		jcbCasos.setModel(new javax.swing.DefaultComboBoxModel<>(
				new String[] { "Caso 1", "Caso 2", "Caso 3", "Caso 4", "Caso 5", "Todos" }));
		jcbModulo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Contacto Softesting" }));

		jrbOpcionCasoUnico = new JRadioButton("Caso individual");
		jrbOpcionCasoUnico.setSelected(true);

		jrbMultiplesCasos = new JRadioButton("Multiples Casos");
		jrbMultiplesCasos.setSelected(false);

		jrbOpcionCasoUnico.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jrbMultiplesCasos.setSelected(false);
				txtMultiplesCasos.setEnabled(false);
				jcbCasos.setEnabled(true);
			}
		});

		jrbMultiplesCasos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jrbOpcionCasoUnico.setSelected(false);
				txtMultiplesCasos.setEnabled(true);
				jcbCasos.setEnabled(false);
			}
		});

		txtMultiplesCasos = new JTextField();
		txtMultiplesCasos.setEnabled(false);
		txtMultiplesCasos.setColumns(10);

		JLabel lblNewLabel = new JLabel("Ejemplo : 1,2,3, separdo por comas los casos que desea correr.");

		javax.swing.GroupLayout jpConfiguracionCasosLayout = new javax.swing.GroupLayout(jpConfiguracionCasos);
		jpConfiguracionCasosLayout
				.setHorizontalGroup(jpConfiguracionCasosLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(jpConfiguracionCasosLayout.createSequentialGroup()
								.addGroup(
										jpConfiguracionCasosLayout
												.createParallelGroup(Alignment.LEADING)
												.addGroup(jpConfiguracionCasosLayout.createSequentialGroup()
														.addComponent(jlSeleccionModulo).addGap(18)
														.addComponent(
																jcbModulo, GroupLayout.PREFERRED_SIZE, 272,
																GroupLayout.PREFERRED_SIZE))
												.addGroup(jpConfiguracionCasosLayout.createSequentialGroup()
														.addGroup(jpConfiguracionCasosLayout
																.createParallelGroup(Alignment.LEADING)
																.addComponent(jrbOpcionCasoUnico)
																.addComponent(jrbMultiplesCasos))
														.addGap(22)
														.addGroup(jpConfiguracionCasosLayout
																.createParallelGroup(Alignment.LEADING, false)
																.addComponent(txtMultiplesCasos)
																.addComponent(jcbCasos, 0, 272, Short.MAX_VALUE)
																.addComponent(lblNewLabel))))
								.addContainerGap(82, Short.MAX_VALUE)));
		jpConfiguracionCasosLayout.setVerticalGroup(jpConfiguracionCasosLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(jpConfiguracionCasosLayout.createSequentialGroup().addGap(5)
						.addGroup(jpConfiguracionCasosLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jlSeleccionModulo).addComponent(jcbModulo, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(jpConfiguracionCasosLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jcbCasos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(jrbOpcionCasoUnico))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(jpConfiguracionCasosLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jrbMultiplesCasos).addComponent(txtMultiplesCasos,
										GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblNewLabel)));
		jpConfiguracionCasos.setLayout(jpConfiguracionCasosLayout);
	}

	private void seleccionAmbiente() {

		jpSeleccionAmbiente = new javax.swing.JPanel();
		jpSeleccionAmbiente.setBorder(javax.swing.BorderFactory.createTitledBorder("Selección del ambiente"));

		rdbMozillaFirefox = new javax.swing.JRadioButton("Mozilla Firefox");
		rdbGoogleChrome = new javax.swing.JRadioButton("Google Chrome");
		jcbSeleccionAmbiente = new javax.swing.JComboBox<>();
		btnEditar = new javax.swing.JButton("Editar");

		bgNavegadores = new javax.swing.ButtonGroup();
		bgNavegadores.add(rdbMozillaFirefox);
		bgNavegadores.add(rdbGoogleChrome);

		jcbSeleccionAmbiente.setModel(
				new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Pruebas", "Desarrollo" }));
		jcbSeleccionAmbiente.setSelectedIndex(0);
		jcbSeleccionAmbiente.setEnabled(false);

		javax.swing.GroupLayout jpSeleccionAmbienteLayout = new javax.swing.GroupLayout(jpSeleccionAmbiente);
		jpSeleccionAmbienteLayout.setHorizontalGroup(
				jpSeleccionAmbienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jpSeleccionAmbienteLayout.createSequentialGroup()
								.addGroup(jpSeleccionAmbienteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(rdbMozillaFirefox).addComponent(rdbGoogleChrome))
								.addGap(65, 65, 65)
								.addGroup(jpSeleccionAmbienteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jcbSeleccionAmbiente).addComponent(btnEditar))
								.addGap(0, 0, Short.MAX_VALUE)));
		jpSeleccionAmbienteLayout.setVerticalGroup(
				jpSeleccionAmbienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(jpSeleccionAmbienteLayout.createSequentialGroup().addContainerGap()
								.addGroup(jpSeleccionAmbienteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(rdbMozillaFirefox).addComponent(jcbSeleccionAmbiente))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(jpSeleccionAmbienteLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(rdbGoogleChrome).addComponent(btnEditar))
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		jpSeleccionAmbiente.setLayout(jpSeleccionAmbienteLayout);
	}

	private void componentes() {

		encabezado();
		credencialesAcceso();
		configuracionCasos();
		seleccionAmbiente();
		configuracionReporte();

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING,
						layout.createSequentialGroup().addContainerGap(226, Short.MAX_VALUE).addComponent(btnAceptar)
								.addGap(200))
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(Alignment.LEADING, layout.createParallelGroup(Alignment.LEADING)
										.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(
												jpConfiguracionReporte, GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(layout.createSequentialGroup().addGap(125).addComponent(jlTitulo))
										.addGroup(layout.createSequentialGroup().addGap(190).addComponent(jlSubTitulo))
										.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(
												jpSeleccionAmbiente, GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(
												jpCredencialesAcceso, GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)))
								.addGroup(Alignment.LEADING,
										layout.createSequentialGroup().addContainerGap().addComponent(
												jpConfiguracionCasos, GroupLayout.PREFERRED_SIZE, 470,
												Short.MAX_VALUE)))
						.addGap(17)));
		layout.setVerticalGroup(
				layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(jlTitulo)
								.addPreferredGap(ComponentPlacement.RELATED).addComponent(jlSubTitulo).addGap(18)
								.addComponent(jpCredencialesAcceso, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jpConfiguracionCasos, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jpSeleccionAmbiente, GroupLayout.PREFERRED_SIZE, 113,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jpConfiguracionReporte, GroupLayout.PREFERRED_SIZE, 119,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnAceptar)
								.addContainerGap(14, Short.MAX_VALUE)));

		getContentPane().setLayout(layout);

		pack();
	}

	public void setUbicacionReporte(String t) {
		txtUbicacionReporte.setText(t);
	}

	public javax.swing.JButton getbtnAbrirUbicacion() {
		return btnAbrirUbicacion;
	}

	public javax.swing.JButton getBtnAceptar() {
		return btnAceptar;
	}

	public javax.swing.JButton getBtnEditar() {
		return btnEditar;
	}

	public javax.swing.JComboBox<String> getJcbCasos() {
		return jcbCasos;
	}

	public javax.swing.JComboBox<String> getJcbModulo() {
		return jcbModulo;
	}

	public javax.swing.JComboBox<String> getJcbSeleccionAmbiente() {
		return jcbSeleccionAmbiente;
	}

	public javax.swing.JCheckBox getJchPassword() {
		return jchPassword;
	}

	public javax.swing.JLabel getJlVerificacionPassword() {
		return jlVerificacionPassword;
	}

	public javax.swing.JPasswordField getJpsPassword() {
		return jpsPassword;
	}

	public javax.swing.JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public javax.swing.JTextField getTxtNombreReporte() {
		return txtNombreReporte;
	}

	public javax.swing.JTextField getTxtUbicacionReporte() {
		return txtUbicacionReporte;
	}

	public javax.swing.JRadioButton getRdbGoogleChrome() {
		return rdbGoogleChrome;
	}

	public javax.swing.JRadioButton getRdbMozillaFirefox() {
		return rdbMozillaFirefox;
	}

	public JRadioButton getJrbOpcionCasoUnico() {
		return jrbOpcionCasoUnico;
	}

	public JRadioButton getJrbMultiplesCasos() {
		return jrbMultiplesCasos;
	}

	public JTextField getTxtMultiplesCasos() {
		return txtMultiplesCasos;
	}

}
