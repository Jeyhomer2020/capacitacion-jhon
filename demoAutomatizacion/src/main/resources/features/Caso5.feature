# language: es
@Caso5
Característica: Validacion Pagina Contactanos de Softesting.

Escenario: Envio de consulta con email incorrecto
		Dado que el usuario ingresa a la pagina de contacto
		Y El usuario llena campo nombre
		Y el usuario llena campo email con una direccion de correo valido
		Y el usuario llena campo mensaje  
		Cuando el usuario da click en el boton enviar  
		Entonces validar mensaje Muchas gracias por ponerte en contacto con nosotros
		
	